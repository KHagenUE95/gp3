﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace GP3._06_Tools.OdinAttributes
{
	[CreateAssetMenu(fileName = "New Weapon Data", menuName = "GP3/WeaponData", order = 200)]
	public class WeaponData : ScriptableObject
	{
		[BoxGroup("BaseValues")] [SerializeField] [PreviewField(ObjectFieldAlignment.Left, Height = 100)] [HorizontalGroup("BaseValues/Horizontal", 100)] [HideLabel] [PropertyOrder(-2)]
		private Sprite _sprite;
		[SerializeField] [VerticalGroup("BaseValues/Horizontal/Vertical")] 
		private string _weaponName;
		[SerializeField] [TextArea] [VerticalGroup("BaseValues/Horizontal/Vertical")]
		private string _description;
		[SerializeField] [FoldoutGroup("Damage")]
		private float _damage;
		[SerializeField] [FoldoutGroup("Damage")] [PropertyOrder(-1)]
		private bool _applyOverTime;
		[SerializeField] [ShowIf("_applyOverTime")] [FoldoutGroup("Damage")]
		private float _duration;
		[ShowInInspector] [ShowIf("_applyOverTime")] [SuffixLabel("dmg/s")] [Sirenix.OdinInspector.ReadOnly] [FoldoutGroup("Damage")] [PropertyOrder(1)]
		private float DamagePerSecond => _damage / _duration;
		[SerializeField] [FoldoutGroup("Damage")] [EnumToggleButtons] [FoldoutGroup("Damage")] [PropertyOrder(2)] [InfoBox("The damage type is used in combat for damage multipliers. Fire > Ice > Ground > Poison > Water > Fire")]
		private DamageType _damageType;

		[TabGroup("Resources", "Mana")] [SerializeField]
		private bool _isMagical;
		[TabGroup("Resources", "Mana")] [SerializeField] [ProgressBar(0, 100)] [SuffixLabel("%")] [ShowIf("_isMagical")]
		private float _manaUsage;
		[TabGroup("Resources", "Stamina")] [SerializeField] [ProgressBar(0, 100)] [SuffixLabel("%")]
		private float _staminaUsage;

		[TabGroup("FX", "UI")] [PreviewField] [SerializeField] 
		private Sprite _uiSprite;
		[TabGroup("FX", "UI")] [SerializeField] 
		private string _displayName;
		[TabGroup("FX", "SFX")] [SerializeField] 
		private AudioClip _onSlashSFX;
		[TabGroup("FX", "SFX")] [SerializeField]
		private AudioClip _onHitSFX;
		[TabGroup("FX", "VFX")] [SerializeField]
		private ParticleSystem _onSlashVFX;
		[TabGroup("FX", "VFX")] [SerializeField] 
		private ParticleSystem _onHitVFX;

		public string WeaponName => _weaponName;

		public enum DamageType
		{
			Fire,
			Water,
			Ice,
			Ground,
			Poison,
		}

		[PropertySpace(50)]
		[GUIColor(0.05f, 0.85f, 0.85f, 1f)]
		[Button("Spawn Example to UI", ButtonSizes.Large)]
		private void SpawnExample()
		{
			GameObject go = new GameObject();
			go.name = _displayName;
			var image = go.AddComponent<Image>();
			image.sprite = _uiSprite;
			go.transform.SetParent(FindObjectOfType<Canvas>().transform);
		}
	}
}