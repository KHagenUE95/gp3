﻿using System;
using System.ComponentModel;
using UnityEditor;
using UnityEngine;

namespace GP3.Code.Tools
{
	public class TimeEditor : EditorWindow, IHasCustomMenu
	{
		private static bool _showVSync;
		private static bool _showFrameRate;

		[MenuItem("GP3/Time Editor")]
		private static void Init()
		{
			TimeEditor window = GetWindowWithRect<TimeEditor>(new Rect(0, 0, 400, 22 + (EditorPrefs.GetBool("ToggleShowFrameRate", true) ? 22 : 0) + (EditorPrefs.GetBool("ToggleShowVSync", true) ? 22 : 0)));
			window.Show();
		}

		private void OnGUI()
		{
			GUILayout.BeginHorizontal();
			
			float[] timescaleValues = { 0.1f, 0.25f, 0.5f, 1f, 5f, 20f, };
			for (int i = 0; i < timescaleValues.Length; i++)
			{
				float currentTimescaleValue = timescaleValues[i];
				if (GUILayout.Button("" + currentTimescaleValue, GUILayout.Width(50)))
				{
					Time.timeScale = currentTimescaleValue;
				}
			}

			float scaleSlider = EditorGUILayout.Slider(Time.timeScale, 0, 20);
			if (scaleSlider > 0)
			{
				Time.timeScale = scaleSlider;
			}

			GUILayout.EndHorizontal();

			if (_showFrameRate)
			{
				GUILayout.BeginHorizontal();
				int frameRateSlider = (int) EditorGUILayout.Slider("Frame Rate", Application.targetFrameRate, 0, 60);
				if (GUILayout.Button("∞", GUILayout.Width(30)))
				{
					frameRateSlider = 0;
				}

				if (Application.targetFrameRate != frameRateSlider)
				{
					QualitySettings.vSyncCount = 0;
					Application.targetFrameRate = frameRateSlider;
				}

				GUILayout.EndHorizontal();
			}
			
			if (_showVSync)
			{
				int newVSync = (int) EditorGUILayout.Slider("VSync", QualitySettings.vSyncCount, 0, 2);
				if (newVSync != QualitySettings.vSyncCount)
				{
					QualitySettings.vSyncCount = newVSync;
				}
			}
		}

		private void OnEnable()
		{
			_showFrameRate = EditorPrefs.GetBool("ToggleShowFrameRate", true);
			_showVSync = EditorPrefs.GetBool("ToggleShowVSync", true);
			minSize = new Vector2(400, 22 + (_showFrameRate ? 22 : 0) + (_showVSync ? 22 : 0));
		}

		private void OnDisable()
		{
			EditorPrefs.SetBool("ToggleShowFrameRate", _showFrameRate);
			EditorPrefs.SetBool("ToggleShowVSync", _showVSync);
		}

		public void AddItemsToMenu(GenericMenu menu)
		{
			GUIContent contentFrameRate = new GUIContent("Show FrameRate");
			menu.AddItem(contentFrameRate, _showFrameRate, ToggleFrameRate);
			
			GUIContent contentVSync = new GUIContent(_showVSync ? "Hide VSync" : "Show VSync");
			menu.AddItem(contentVSync, false, ToggleVSync);
		}

		private void ToggleVSync()
		{
			_showVSync = !_showVSync;
			minSize = new Vector2(400, 22 + (_showFrameRate ? 22 : 0) + (_showVSync ? 22 : 0));
		}

		private void ToggleFrameRate()
		{
			_showFrameRate = !_showFrameRate;
			minSize = new Vector2(400, 22 + (_showFrameRate ? 22 : 0) + (_showVSync ? 22 : 0));
		}
	}
}