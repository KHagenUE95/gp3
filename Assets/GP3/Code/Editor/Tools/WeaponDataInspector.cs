﻿// using System;
// using GP3._06_Tools.OdinAttributes;
// using UnityEditor;
// using UnityEngine;
//
// namespace GP3.Code.Tools.Odin
// {
// 	[CanEditMultipleObjects]
// 	[CustomEditor(typeof(WeaponData))]
// 	public class WeaponDataInspector : Editor
// 	{
// 		private SerializedProperty _weaponSpriteProperty;
// 		private SerializedProperty _weaponNameProperty;
// 		private SerializedProperty _weaponDescriptionProperty;
// 		private SerializedProperty _damageProperty;
// 		private SerializedProperty _applyOverTimeProperty;
// 		private SerializedProperty _durationProperty;
// 		private SerializedProperty _damageTypeProperty;
// 		private SerializedProperty _isMagicalProperty;
// 		private SerializedProperty _manaUsageProperty;
// 		private SerializedProperty _staminaUsageProperty;
// 		private SerializedProperty _uiSpriteProperty;
// 		private SerializedProperty _displayNameProperty;
// 		private SerializedProperty _onHitSFXProperty;
// 		private SerializedProperty _onSlashSFXProperty;
// 		private SerializedProperty _onHitVFXProperty;
// 		private SerializedProperty _onSlashVFXProperty;
// 		
// 		private bool _showBaseValues = true;
// 		private bool _showDamageValues;
// 		private int _resourceSelectionIndex;
// 		private bool _showMana;
// 		private bool _showStamina;
// 		private int _fxSelectionIndex;
// 		private bool _showUI;
// 		private bool _showSFX;
// 		private bool _showVFX;
//
// 		private void OnEnable()
// 		{
// 			_weaponSpriteProperty = serializedObject.FindProperty("_sprite");
// 			_weaponNameProperty = serializedObject.FindProperty("_weaponName");
// 			_weaponDescriptionProperty = serializedObject.FindProperty("_description");
// 			_damageProperty = serializedObject.FindProperty("_damage");
// 			_applyOverTimeProperty = serializedObject.FindProperty("_applyOverTime");
// 			_durationProperty = serializedObject.FindProperty("_duration");
// 			_damageTypeProperty = serializedObject.FindProperty("_damageType");
// 			_isMagicalProperty = serializedObject.FindProperty("_isMagical");
// 			_manaUsageProperty = serializedObject.FindProperty("_manaUsage");
// 			_staminaUsageProperty = serializedObject.FindProperty("_staminaUsage");
// 			_uiSpriteProperty = serializedObject.FindProperty("_uiSprite");
// 			_displayNameProperty = serializedObject.FindProperty("_displayName");
// 			_onHitSFXProperty = serializedObject.FindProperty("_onHitSFX");
// 			_onSlashSFXProperty = serializedObject.FindProperty("_onSlashSFX");
// 			_onHitVFXProperty = serializedObject.FindProperty("_onHitVFX");
// 			_onSlashVFXProperty = serializedObject.FindProperty("_onSlashVFX");
// 		}
//
// 		public override void OnInspectorGUI()
// 		{
// 			base.OnInspectorGUI();
// 			
// 			// _showBaseValues = EditorGUILayout.BeginFoldoutHeaderGroup(_showBaseValues, "Base Values");
// 			// if (_showBaseValues)
// 			// {
// 			// 	EditorGUILayout.BeginHorizontal();
// 			// 	EditorGUILayout.ObjectField(_weaponSpriteProperty);
// 			// 	EditorGUILayout.BeginVertical();
// 			// 	_weaponNameProperty.stringValue = EditorGUILayout.TextField("Weapon Name", _weaponNameProperty.stringValue);
// 			// 	EditorGUILayout.LabelField("Description");
// 			// 	_weaponDescriptionProperty.stringValue = EditorGUILayout.TextArea(_weaponDescriptionProperty.stringValue);
// 			// 	EditorGUILayout.EndVertical();
// 			// 	EditorGUILayout.EndHorizontal();
// 			// }
// 			//
// 			// EditorGUILayout.EndFoldoutHeaderGroup();
// 			//
// 			// _showDamageValues = EditorGUILayout.BeginFoldoutHeaderGroup(_showDamageValues, "Damage Values");
// 			// if (_showDamageValues)
// 			// {
// 			// 	_applyOverTimeProperty.boolValue = EditorGUILayout.Toggle("Apply Over Time", _applyOverTimeProperty.boolValue);
// 			// 	_damageProperty.floatValue = EditorGUILayout.FloatField("Damage", _damageProperty.floatValue);
// 			//
// 			// 	if (_applyOverTimeProperty.boolValue)
// 			// 	{
// 			// 		_durationProperty.floatValue = EditorGUILayout.FloatField("Duration", _durationProperty.floatValue);
// 			// 		EditorGUI.BeginDisabledGroup(true);
// 			// 		EditorGUILayout.FloatField("DPS", _damageProperty.floatValue / _durationProperty.floatValue);
// 			// 		EditorGUI.EndDisabledGroup();
// 			// 	}
// 			//
// 			// 	EditorGUILayout.BeginHorizontal();
// 			// 	EditorGUILayout.LabelField("Damage Type");
// 			// 	_damageTypeProperty.enumValueIndex = GUILayout.Toolbar(_damageTypeProperty.enumValueIndex, Enum.GetNames(typeof(WeaponData.DamageType)));
// 			// 	EditorGUILayout.EndHorizontal();
// 			// }
// 			//
// 			// EditorGUILayout.EndFoldoutHeaderGroup();
// 			//
// 			//
// 			// EditorGUILayout.BeginHorizontal();
// 			// _resourceSelectionIndex = GUILayout.Toolbar(_resourceSelectionIndex, new[]{"Mana","Stamina",});
// 			// _showMana = _resourceSelectionIndex == 0;
// 			// _showStamina = _resourceSelectionIndex == 1;
// 			// EditorGUILayout.EndHorizontal();
// 			// EditorGUILayout.BeginVertical();
// 			// if (_showMana)
// 			// {
// 			// 	_isMagicalProperty.boolValue = EditorGUILayout.Toggle("IsMagical", _isMagicalProperty.boolValue);
// 			// 	if (_isMagicalProperty.boolValue)
// 			// 	{
// 			// 		_manaUsageProperty.floatValue = EditorGUILayout.Slider("Mana Usage", _manaUsageProperty.floatValue, 0, 100);
// 			// 		EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(), _manaUsageProperty.floatValue / 100f, "Mana Usage");
// 			// 	}
// 			// }
// 			//
// 			// if (_showStamina)
// 			// {
// 			// 	_staminaUsageProperty.floatValue = EditorGUILayout.Slider("Stamina Usage", _staminaUsageProperty.floatValue, 0, 100);
// 			// 	EditorGUI.ProgressBar(EditorGUILayout.GetControlRect(), _staminaUsageProperty.floatValue / 100f, "Stamina Usage");
// 			// }
// 			// EditorGUILayout.EndVertical();
// 			//
// 			//
// 			// EditorGUILayout.BeginHorizontal();
// 			// _fxSelectionIndex = GUILayout.Toolbar(_fxSelectionIndex, new[]{"UI","SFX","VFX",});
// 			// _showUI = _fxSelectionIndex == 0;
// 			// _showSFX = _fxSelectionIndex == 1;
// 			// _showVFX = _fxSelectionIndex == 2;
// 			// EditorGUILayout.EndHorizontal();
// 			// EditorGUILayout.BeginVertical();
// 			// if (_showUI)
// 			// {
// 			// 	EditorGUILayout.ObjectField(_uiSpriteProperty);
// 			// 	_displayNameProperty.stringValue = EditorGUILayout.TextField("Display Name", _displayNameProperty.stringValue);
// 			// }
// 			//
// 			// if (_showSFX)
// 			// {
// 			// 	EditorGUILayout.ObjectField(_onHitSFXProperty);
// 			// 	EditorGUILayout.ObjectField(_onSlashSFXProperty);
// 			// }
// 			//
// 			// if (_showVFX)
// 			// {
// 			// 	EditorGUILayout.ObjectField(_onHitVFXProperty);
// 			// 	EditorGUILayout.ObjectField(_onSlashVFXProperty);
// 			// }
// 			// EditorGUILayout.EndVertical();
// 			//
// 			// serializedObject.ApplyModifiedProperties();
// 		}
// 	}
// }